#![feature(no_core)]
#![no_core]

mod style {
    pub struct Color;
}

pub use style::Color;
pub use style::Color as Colour;
