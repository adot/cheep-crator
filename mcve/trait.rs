pub trait Get {
    type Target;
    fn get(&self) -> &Self::Target;
}

pub trait LoadNum {
    fn load_num(&self) -> i32;
}

impl<A: LoadNum, P: Get<Target = A>> LoadNum for P {
    fn load_num(&self) -> i32 {
        self.get().load_num()
    }
}

pub struct Wrapper<T>(T);
impl<T> Get for Wrapper<T> {
    type Target = T;
    fn get(&self) -> &T {
        &self.0
    }
}
