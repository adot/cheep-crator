#!/bin/bash


while IFS= read -r i
do	
	cd vendor/"$i" || exit
	cargo +stage1 rustdoc -- --output-format json
	cd ../..
done < <(cat failing)
