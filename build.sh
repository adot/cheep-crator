#!/bin/bash
foreach () {
	cargo +stage1 rustdoc -j1 -- --output-format json > /dev/null 2>&1 	
	if [ $? -ne 0 ]; then
		basename $(pwd)
	fi
}

wrapper () {
	cd "$1" || exit
	foreach
	}

export -f wrapper
export -f foreach

for i in $(fd -d 1 . vendor)
do
	sem -j10 wrapper "$i"
done
