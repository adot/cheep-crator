#![allow(unknown_lints, bare_trait_objects, renamed_and_removed_lints)]

pub mod access;
mod as_raw;
pub mod cache;
mod compile_fail_tests;
mod debt;
pub mod gen_lock;
mod ref_cnt;
#[cfg(feature = "weak")]
mod weak;

use std::fmt::{Debug, Display, Formatter, Result as FmtResult};
use std::isize;
use std::marker::PhantomData;
use std::mem::{self, ManuallyDrop};
use std::ops::Deref;
use std::process;
use std::ptr;
use std::sync::atomic::{self, AtomicPtr, AtomicUsize, Ordering};
use std::sync::Arc;
use std::thread;

use access::{Access, Map};
use as_raw::AsRaw;
pub use cache::Cache;
use debt::Debt;
use gen_lock::{Global, LockStorage, PrivateUnsharded, GEN_CNT};
pub use ref_cnt::RefCnt;

const MAX_GUARDS: usize = (isize::MAX) as usize;

struct GenLock<'a> {
    slot: &'a AtomicUsize,
}

impl<'a> GenLock<'a> {
    fn new<S: LockStorage + 'a>(signal_safe: SignalSafety, lock_storage: &'a S) -> Self {
        let shard = match signal_safe {
            SignalSafety::Safe => 0,
            SignalSafety::Unsafe => lock_storage.choose_shard(),
        };
        let gen = lock_storage.gen_idx().load(Ordering::Relaxed) % GEN_CNT;

        let slot = &lock_storage.shards().as_ref()[shard].0[gen];
        let old = slot.fetch_add(1, Ordering::SeqCst);

        if old > MAX_GUARDS {
            process::abort();
        }
        GenLock { slot }
    }

    fn unlock(self) {
        self.slot.fetch_sub(1, Ordering::AcqRel);

        mem::forget(self);
    }
}

#[cfg(debug_assertions)]
impl<'a> Drop for GenLock<'a> {
    fn drop(&mut self) {
        unreachable!("Forgot to unlock generation");
    }
}

enum Protection<'l> {
    Unprotected,

    Debt(&'static Debt),

    Lock(GenLock<'l>),
}

impl<'l> From<Option<&'static Debt>> for Protection<'l> {
    fn from(debt: Option<&'static Debt>) -> Self {
        match debt {
            Some(d) => Protection::Debt(d),
            None => Protection::Unprotected,
        }
    }
}

pub struct Guard<'l, T: RefCnt> {
    inner: ManuallyDrop<T>,
    protection: Protection<'l>,
}

impl<'a, T: RefCnt> Guard<'a, T> {
    fn new(ptr: *const T::Base, protection: Protection<'a>) -> Guard<'a, T> {
        Guard {
            inner: ManuallyDrop::new(unsafe { T::from_ptr(ptr) }),
            protection,
        }
    }

    #[cfg_attr(feature = "cargo-clippy", allow(wrong_self_convention))]
    #[inline]
    pub fn into_inner(mut lease: Self) -> T {
        match mem::replace(&mut lease.protection, Protection::Unprotected) {
            Protection::Unprotected => (),

            Protection::Debt(debt) => {
                T::inc(&lease.inner);
                let ptr = T::as_ptr(&lease.inner);
                if !debt.pay::<T>(ptr) {
                    unsafe { T::dec(ptr) };
                }
            }

            Protection::Lock(lock) => {
                T::inc(&lease.inner);
                lock.unlock();
            }
        }

        let inner = unsafe { ptr::read(lease.inner.deref()) };
        mem::forget(lease);
        inner
    }

    pub fn from_inner(inner: T) -> Self {
        Guard {
            inner: ManuallyDrop::new(inner),
            protection: Protection::Unprotected,
        }
    }
}

impl<'a, T: RefCnt> Deref for Guard<'a, T> {
    type Target = T;
    #[inline]
    fn deref(&self) -> &T {
        self.inner.deref()
    }
}

impl<'a, T: Debug + RefCnt> Debug for Guard<'a, T> {
    fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
        self.deref().fmt(formatter)
    }
}

impl<'a, T: Display + RefCnt> Display for Guard<'a, T> {
    fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
        self.deref().fmt(formatter)
    }
}

impl<'a, T: RefCnt> Drop for Guard<'a, T> {
    #[inline]
    fn drop(&mut self) {
        match mem::replace(&mut self.protection, Protection::Unprotected) {
            Protection::Unprotected => (),

            Protection::Debt(debt) => {
                let ptr = T::as_ptr(&self.inner);
                if debt.pay::<T>(ptr) {
                    return;
                }
            }

            Protection::Lock(lock) => {
                lock.unlock();
                return;
            }
        }

        unsafe { ManuallyDrop::drop(&mut self.inner) };
    }
}

#[cfg_attr(feature = "cargo-clippy", allow(needless_pass_by_value))]
fn ptr_eq<Base, A, B>(a: A, b: B) -> bool
where
    A: AsRaw<Base>,
    B: AsRaw<Base>,
{
    let a = a.as_raw();
    let b = b.as_raw();
    ptr::eq(a, b)
}

#[derive(Copy, Clone)]
enum SignalSafety {
    Safe,
    Unsafe,
}

const YIELD_EVERY: usize = 16;

pub struct ArcSwapAny<T: RefCnt, S: LockStorage = Global> {
    ptr: AtomicPtr<T::Base>,

    _phantom_arc: PhantomData<T>,

    lock_storage: S,
}

impl<T: RefCnt, S: LockStorage> From<T> for ArcSwapAny<T, S> {
    fn from(val: T) -> Self {
        let ptr = T::into_ptr(val);
        Self {
            ptr: AtomicPtr::new(ptr),
            _phantom_arc: PhantomData,
            lock_storage: S::default(),
        }
    }
}

impl<T: RefCnt, S: LockStorage> Drop for ArcSwapAny<T, S> {
    fn drop(&mut self) {
        let ptr = *self.ptr.get_mut();

        self.wait_for_readers(ptr);

        unsafe { T::dec(ptr) };
    }
}

impl<T: RefCnt, S: LockStorage> Clone for ArcSwapAny<T, S> {
    fn clone(&self) -> Self {
        Self::from(self.load_full())
    }
}

impl<T, S: LockStorage> Debug for ArcSwapAny<T, S>
where
    T: Debug + RefCnt,
{
    fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
        formatter
            .debug_tuple("ArcSwapAny")
            .field(&self.load())
            .finish()
    }
}

impl<T, S: LockStorage> Display for ArcSwapAny<T, S>
where
    T: Display + RefCnt,
{
    fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
        self.load().fmt(formatter)
    }
}

impl<T: RefCnt + Default, S: LockStorage> Default for ArcSwapAny<T, S> {
    fn default() -> Self {
        Self::new(T::default())
    }
}

impl<T: RefCnt, S: LockStorage> ArcSwapAny<T, S> {
    pub fn new(val: T) -> Self {
        Self::from(val)
    }

    pub fn into_inner(mut self) -> T {
        let ptr = *self.ptr.get_mut();

        self.wait_for_readers(ptr);
        mem::forget(self);
        unsafe { T::from_ptr(ptr) }
    }

    pub fn load_full(&self) -> T {
        Guard::into_inner(self.load())
    }

    #[inline]
    fn lock_internal(&self, signal_safe: SignalSafety) -> Guard<'_, T> {
        let gen = GenLock::new(signal_safe, &self.lock_storage);
        let ptr = self.ptr.load(Ordering::Acquire);

        Guard::new(ptr, Protection::Lock(gen))
    }

    pub fn load_signal_safe(&self) -> Guard<'_, T> {
        self.lock_internal(SignalSafety::Safe)
    }

    #[inline]
    fn load_fallible(&self) -> Option<Guard<'static, T>> {
        let ptr = self.ptr.load(Ordering::Relaxed);

        let debt = Debt::new(ptr as usize)?;

        let confirm = self.ptr.load(Ordering::Acquire);
        if ptr == confirm {
            Some(Guard::new(ptr, Protection::Debt(debt)))
        } else if debt.pay::<T>(ptr) {
            None
        } else {
            Some(Guard::new(ptr, Protection::Unprotected))
        }
    }

    #[inline]
    pub fn load(&self) -> Guard<'static, T> {
        self.load_fallible().unwrap_or_else(|| {
            let locked = self.lock_internal(SignalSafety::Unsafe);

            Guard::from_inner(Guard::into_inner(locked))
        })
    }

    pub fn store(&self, val: T) {
        drop(self.swap(val));
    }

    pub fn swap(&self, new: T) -> T {
        let new = T::into_ptr(new);

        let old = self.ptr.swap(new, Ordering::SeqCst);
        self.wait_for_readers(old);
        unsafe { T::from_ptr(old) }
    }

    pub fn compare_and_swap<C: AsRaw<T::Base>>(&self, current: C, new: T) -> Guard<T> {
        let cur_ptr = current.as_raw();
        let new = T::into_ptr(new);

        let gen = GenLock::new(SignalSafety::Unsafe, &self.lock_storage);

        let previous_ptr = self.ptr.compare_and_swap(cur_ptr, new, Ordering::SeqCst);
        let swapped = ptr::eq(cur_ptr, previous_ptr);

        drop(current);

        let debt = if swapped {
            None
        } else {
            let debt = Debt::new(previous_ptr as usize);
            if debt.is_none() {
                let previous = unsafe { T::from_ptr(previous_ptr) };
                T::inc(&previous);
                T::into_ptr(previous);
            }
            debt
        };

        gen.unlock();

        if swapped {
            self.wait_for_readers(previous_ptr);
        } else {
            unsafe { T::dec(new) };
        }

        Guard::new(previous_ptr, debt.into())
    }

    fn wait_for_readers(&self, old: *const T::Base) {
        let mut seen_group = [false; GEN_CNT];
        let mut iter = 0usize;

        loop {
            let gen = self.lock_storage.gen_idx().load(Ordering::Relaxed);
            let groups = self
                .lock_storage
                .shards()
                .as_ref()
                .iter()
                .fold([0, 0], |[a1, a2], s| {
                    let [v1, v2] = s.snapshot();
                    [a1 + v1, a2 + v2]
                });

            let next_gen = gen.wrapping_add(1);
            if groups[next_gen % GEN_CNT] == 0 {
                self.lock_storage
                    .gen_idx()
                    .compare_and_swap(gen, next_gen, Ordering::Relaxed);
            }
            for i in 0..GEN_CNT {
                seen_group[i] = seen_group[i] || (groups[i] == 0);
            }

            if seen_group.iter().all(|seen| *seen) {
                break;
            }

            iter = iter.wrapping_add(1);
            if cfg!(not(miri)) {
                if iter % YIELD_EVERY == 0 {
                    thread::yield_now();
                } else {
                    atomic::spin_loop_hint();
                }
            }
        }
        Debt::pay_all::<T>(old);
    }

    pub fn rcu<R, F>(&self, mut f: F) -> T
    where
        F: FnMut(&T) -> R,
        R: Into<T>,
    {
        let mut cur = self.load();
        loop {
            let new = f(&cur).into();
            let prev = self.compare_and_swap(&cur, new);
            let swapped = ptr_eq(&cur, &prev);
            if swapped {
                return Guard::into_inner(prev);
            } else {
                cur = prev;
            }
        }
    }

    pub fn map<I, R, F>(&self, f: F) -> Map<&Self, I, F>
    where
        F: Fn(&I) -> &R + Clone,
        Self: Access<I>,
    {
        Map::new(self, f)
    }
}

pub type ArcSwap<T> = ArcSwapAny<Arc<T>>;

impl<T, S: LockStorage> ArcSwapAny<Arc<T>, S> {
    pub fn from_pointee(val: T) -> Self {
        Self::from(Arc::new(val))
    }

    pub fn rcu_unwrap<R, F>(&self, mut f: F) -> T
    where
        F: FnMut(&T) -> R,
        R: Into<Arc<T>>,
    {
        let mut wrapped = self.rcu(|prev| f(&*prev));
        loop {
            match Arc::try_unwrap(wrapped) {
                Ok(val) => return val,
                Err(w) => {
                    wrapped = w;
                    thread::yield_now();
                }
            }
        }
    }
}

pub type ArcSwapOption<T> = ArcSwapAny<Option<Arc<T>>>;

impl<T, S: LockStorage> ArcSwapAny<Option<Arc<T>>, S> {
    pub fn from_pointee<V: Into<Option<T>>>(val: V) -> Self {
        Self::new(val.into().map(Arc::new))
    }

    pub fn empty() -> Self {
        Self::new(None)
    }
}

pub type IndependentArcSwap<T> = ArcSwapAny<Arc<T>, PrivateUnsharded>;

#[cfg(feature = "weak")]
pub type ArcSwapWeak<T> = ArcSwapAny<std::sync::Weak<T>>;

#[cfg(test)]
mod tests {
    extern crate crossbeam_utils;

    use std::panic;
    use std::sync::atomic::AtomicUsize;
    use std::sync::Barrier;

    use self::crossbeam_utils::thread;

    use super::*;

    #[test]
    fn publish() {
        for _ in 0..100 {
            let config = ArcSwap::<String>::default();
            let ended = AtomicUsize::new(0);
            thread::scope(|scope| {
                for _ in 0..20 {
                    scope.spawn(|_| loop {
                        let cfg = config.load_full();
                        if !cfg.is_empty() {
                            assert_eq!(*cfg, "New configuration");
                            ended.fetch_add(1, Ordering::Relaxed);
                            return;
                        }
                        atomic::spin_loop_hint();
                    });
                }
                scope.spawn(|_| {
                    let new_conf = Arc::new("New configuration".to_owned());
                    config.store(new_conf);
                });
            })
            .unwrap();
            assert_eq!(20, ended.load(Ordering::Relaxed));
            let arc = config.load_full();
            assert_eq!(2, Arc::strong_count(&arc));
            assert_eq!(0, Arc::weak_count(&arc));
        }
    }

    #[test]
    fn swap_load() {
        for _ in 0..100 {
            let arc = Arc::new(42);
            let arc_swap = ArcSwap::from(Arc::clone(&arc));
            assert_eq!(42, **arc_swap.load());

            assert_eq!(42, **arc_swap.load());

            let new_arc = Arc::new(0);
            assert_eq!(42, *arc_swap.swap(Arc::clone(&new_arc)));
            assert_eq!(0, **arc_swap.load());

            let loaded = arc_swap.load_full();
            assert_eq!(3, Arc::strong_count(&loaded));
            assert_eq!(0, Arc::weak_count(&loaded));

            assert_eq!(1, Arc::strong_count(&arc));
            assert_eq!(0, Arc::weak_count(&arc));
        }
    }

    #[test]
    fn multi_writers() {
        let first_value = Arc::new((0, 0));
        let shared = ArcSwap::from(Arc::clone(&first_value));
        const WRITER_CNT: usize = 2;
        const READER_CNT: usize = 3;
        const ITERATIONS: usize = 100;
        const SEQ: usize = 50;
        let barrier = Barrier::new(READER_CNT + WRITER_CNT);
        thread::scope(|scope| {
            for w in 0..WRITER_CNT {
                let barrier = &barrier;
                let shared = &shared;
                let first_value = &first_value;
                scope.spawn(move |_| {
                    for _ in 0..ITERATIONS {
                        barrier.wait();
                        shared.store(Arc::clone(&first_value));
                        barrier.wait();
                        for i in 0..SEQ {
                            shared.store(Arc::new((w, i + 1)));
                        }
                    }
                });
            }
            for _ in 0..READER_CNT {
                scope.spawn(|_| {
                    for _ in 0..ITERATIONS {
                        barrier.wait();
                        barrier.wait();
                        let mut previous = [0; 2];
                        let mut last = Arc::clone(&first_value);
                        loop {
                            let cur = shared.load();
                            if Arc::ptr_eq(&last, &cur) {
                                atomic::spin_loop_hint();
                                continue;
                            }
                            let (w, s) = **cur;
                            assert!(previous[w] < s);
                            previous[w] = s;
                            last = Guard::into_inner(cur);
                            if s == SEQ {
                                break;
                            }
                        }
                    }
                });
            }
        })
        .unwrap();
    }

    #[test]

    fn cas_ref_cnt() {
        const ITERATIONS: usize = 50;
        let shared = ArcSwap::from(Arc::new(0));
        for i in 0..ITERATIONS {
            let orig = shared.load_full();
            assert_eq!(i, *orig);
            if i % 2 == 1 {
                assert_eq!(2, Arc::strong_count(&orig));
            }
            let n1 = Arc::new(i + 1);

            let fillup = || {
                if i % 2 == 0 {
                    Some((0..50).map(|_| shared.load()).collect::<Vec<_>>())
                } else {
                    None
                }
            };
            let guards = fillup();

            let prev = shared.compare_and_swap(&orig, Arc::clone(&n1));
            assert!(ptr_eq(&orig, &prev));
            drop(guards);

            assert_eq!(2, Arc::strong_count(&orig));

            assert_eq!(2, Arc::strong_count(&n1));
            assert_eq!(i + 1, **shared.load());
            let n2 = Arc::new(i);
            drop(prev);
            let guards = fillup();

            let prev = Guard::into_inner(shared.compare_and_swap(&orig, Arc::clone(&n2)));
            drop(guards);
            assert!(ptr_eq(&n1, &prev));

            assert_eq!(1, Arc::strong_count(&orig));

            assert_eq!(3, Arc::strong_count(&n1));

            assert_eq!(1, Arc::strong_count(&n2));
            assert_eq!(i + 1, **shared.load());
        }

        let a = shared.load_full();

        assert_eq!(2, Arc::strong_count(&a));
        drop(shared);

        assert_eq!(1, Arc::strong_count(&a));
    }

    #[test]

    fn rcu() {
        const ITERATIONS: usize = 50;
        const THREADS: usize = 10;
        let shared = ArcSwap::from(Arc::new(0));
        thread::scope(|scope| {
            for _ in 0..THREADS {
                scope.spawn(|_| {
                    for _ in 0..ITERATIONS {
                        shared.rcu(|old| **old + 1);
                    }
                });
            }
        })
        .unwrap();
        assert_eq!(THREADS * ITERATIONS, **shared.load());
    }

    #[test]

    fn rcu_unwrap() {
        const ITERATIONS: usize = 50;
        const THREADS: usize = 10;
        let shared = ArcSwap::from(Arc::new(0));
        thread::scope(|scope| {
            for _ in 0..THREADS {
                scope.spawn(|_| {
                    for _ in 0..ITERATIONS {
                        shared.rcu_unwrap(|old| *old + 1);
                    }
                });
            }
        })
        .unwrap();
        assert_eq!(THREADS * ITERATIONS, **shared.load());
    }

    #[test]
    fn nulls() {
        let shared = ArcSwapOption::from(Some(Arc::new(0)));
        let orig = shared.swap(None);
        assert_eq!(1, Arc::strong_count(&orig.unwrap()));
        let null = shared.load();
        assert!(null.is_none());
        let a = Arc::new(42);
        let orig = shared.compare_and_swap(ptr::null(), Some(Arc::clone(&a)));
        assert!(orig.is_none());
        assert_eq!(2, Arc::strong_count(&a));
        let orig = Guard::into_inner(shared.compare_and_swap(&None::<Arc<_>>, None));
        assert_eq!(3, Arc::strong_count(&a));
        assert!(ptr_eq(&a, &orig));
    }

    #[test]
    fn recursive() {
        let shared = ArcSwap::from(Arc::new(0));

        shared.rcu(|i| {
            if **i < 10 {
                shared.rcu(|i| **i + 1);
            }
            **i
        });
        assert_eq!(10, **shared.load());
        assert_eq!(2, Arc::strong_count(&shared.load_full()));
    }

    #[test]
    fn rcu_panic() {
        let shared = ArcSwap::from(Arc::new(0));
        assert!(panic::catch_unwind(|| shared.rcu(|_| -> usize { panic!() })).is_err());
        assert_eq!(1, Arc::strong_count(&shared.swap(Arc::new(42))));
    }

    #[test]
    fn load_cnt() {
        let a = Arc::new(0);
        let shared = ArcSwap::from(Arc::clone(&a));

        assert_eq!(2, Arc::strong_count(&a));
        let guard = shared.load();
        assert_eq!(0, **guard);

        assert_eq!(2, Arc::strong_count(&a));
        let guard_2 = shared.load();

        shared.store(Arc::new(1));

        assert_eq!(3, Arc::strong_count(&a));

        drop(guard_2);
        assert_eq!(2, Arc::strong_count(&a));
        let _b = Arc::clone(&guard);
        assert_eq!(3, Arc::strong_count(&a));

        drop(guard);
        assert_eq!(2, Arc::strong_count(&a));
        let guard = shared.load();
        assert_eq!(1, **guard);
        drop(shared);

        assert_eq!(1, **guard);
        let ptr = Arc::clone(&guard);

        assert_eq!(2, Arc::strong_count(&ptr));
        drop(guard);
        assert_eq!(1, Arc::strong_count(&ptr));
    }

    #[test]
    fn lease_overflow() {
        let a = Arc::new(0);
        let shared = ArcSwap::from(Arc::clone(&a));
        assert_eq!(2, Arc::strong_count(&a));
        let mut guards = (0..1000).map(|_| shared.load()).collect::<Vec<_>>();
        let count = Arc::strong_count(&a);
        assert!(count > 2);
        let guard = shared.load();
        assert_eq!(count + 1, Arc::strong_count(&a));
        drop(guard);
        assert_eq!(count, Arc::strong_count(&a));

        guards.swap_remove(0);

        let _guard = shared.load();
        assert_eq!(count, Arc::strong_count(&a));
    }

    #[test]
    fn load_null() {
        let shared = ArcSwapOption::<usize>::default();
        let guard = shared.load();
        assert!(guard.is_none());
        shared.store(Some(Arc::new(42)));
        assert_eq!(42, **shared.load().as_ref().unwrap());
    }

    #[test]
    fn from_into() {
        let a = Arc::new(42);
        let shared = ArcSwap::new(a);
        let guard = shared.load();
        let a = shared.into_inner();
        assert_eq!(42, *a);
        assert_eq!(2, Arc::strong_count(&a));
        drop(guard);
        assert_eq!(1, Arc::strong_count(&a));
    }

    #[derive(Default)]
    struct ReportDrop(Arc<AtomicUsize>);
    impl Drop for ReportDrop {
        fn drop(&mut self) {
            self.0.fetch_add(1, Ordering::Relaxed);
        }
    }

    const ITERATIONS: usize = 50;

    #[test]
    fn guard_drop_in_thread() {
        for _ in 0..ITERATIONS {
            let cnt = Arc::new(AtomicUsize::new(0));

            let shared = ArcSwap::from_pointee(ReportDrop(cnt.clone()));
            assert_eq!(cnt.load(Ordering::Relaxed), 0, "Dropped prematurely");

            let sync = Barrier::new(2);

            thread::scope(|scope| {
                scope.spawn(|_| {
                    let guard = shared.load();
                    sync.wait();

                    sync.wait();
                    drop(guard);
                    assert_eq!(cnt.load(Ordering::Relaxed), 1, "Value not dropped");

                    sync.wait();
                });

                scope.spawn(|_| {
                    sync.wait();
                    shared.store(Default::default());
                    assert_eq!(cnt.load(Ordering::Relaxed), 0, "Dropped while still in use");

                    sync.wait();

                    sync.wait();
                    assert_eq!(cnt.load(Ordering::Relaxed), 1, "Value not dropped");
                });
            })
            .unwrap();
        }
    }

    #[test]
    fn guard_drop_in_another_thread() {
        for _ in 0..ITERATIONS {
            let cnt = Arc::new(AtomicUsize::new(0));
            let shared = ArcSwap::from_pointee(ReportDrop(cnt.clone()));
            assert_eq!(cnt.load(Ordering::Relaxed), 0, "Dropped prematurely");
            let guard = shared.load();

            drop(shared);
            assert_eq!(cnt.load(Ordering::Relaxed), 0, "Dropped prematurely");

            thread::scope(|scope| {
                scope.spawn(|_| {
                    drop(guard);
                });
            })
            .unwrap();

            assert_eq!(cnt.load(Ordering::Relaxed), 1, "Not dropped");
        }
    }

    #[test]
    fn signal_drop_in_another_thread() {
        for _ in 0..ITERATIONS {
            let cnt = Arc::new(AtomicUsize::new(0));
            let shared = ArcSwap::from_pointee(ReportDrop(cnt.clone()));
            assert_eq!(cnt.load(Ordering::Relaxed), 0, "Dropped prematurely");
            let guard = shared.load_signal_safe();

            thread::scope(|scope| {
                scope.spawn(|_| {
                    drop(guard);
                });

                assert_eq!(cnt.load(Ordering::Relaxed), 0, "Dropped prematurely");
                shared.swap(Default::default());
                assert_eq!(cnt.load(Ordering::Relaxed), 1, "Not dropped");
            })
            .unwrap();
        }
    }

    #[test]
    fn load_option() {
        let shared = ArcSwapOption::from_pointee(42);

        let opt: Option<_> = Guard::into_inner(shared.load());
        assert_eq!(42, *opt.unwrap());

        shared.store(None);
        assert!(shared.load().is_none());
    }

    #[test]
    fn debug_impl() {
        let shared = ArcSwap::from_pointee(42);
        assert_eq!("ArcSwapAny(42)", &format!("{:?}", shared));
        assert_eq!("42", &format!("{:?}", shared.load()));
    }

    #[test]
    fn display_impl() {
        let shared = ArcSwap::from_pointee(42);
        assert_eq!("42", &format!("{}", shared));
        assert_eq!("42", &format!("{}", shared.load()));
    }

    fn _check_stuff_is_send_sync() {
        let shared = ArcSwap::from_pointee(42);
        let moved = ArcSwap::from_pointee(42);
        let shared_ref = &shared;
        let lease = shared.load();
        let lease_ref = &lease;
        let lease = shared.load();
        let guard = shared.load_signal_safe();
        let guard_ref = &guard;
        let guard = shared.load_signal_safe();
        thread::scope(|s| {
            s.spawn(move |_| {
                let _ = guard;
                let _ = guard_ref;
                let _ = lease;
                let _ = lease_ref;
                let _ = shared_ref;
                let _ = moved;
            });
        })
        .unwrap();
    }
}
