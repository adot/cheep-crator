use super::{Guard, RefCnt};

pub trait AsRaw<T> {
    fn as_raw(&self) -> *mut T;
}

impl<'a, T: RefCnt> AsRaw<T::Base> for &'a T {
    fn as_raw(&self) -> *mut T::Base {
        T::as_ptr(self)
    }
}

impl<'a, T: RefCnt> AsRaw<T::Base> for &'a Guard<'a, T> {
    fn as_raw(&self) -> *mut T::Base {
        T::as_ptr(&self.inner)
    }
}

impl<'a, T: RefCnt> AsRaw<T::Base> for Guard<'a, T> {
    fn as_raw(&self) -> *mut T::Base {
        T::as_ptr(&self.inner)
    }
}

impl<T> AsRaw<T> for *mut T {
    fn as_raw(&self) -> *mut T {
        *self
    }
}

impl<T> AsRaw<T> for *const T {
    fn as_raw(&self) -> *mut T {
        *self as *mut T
    }
}
